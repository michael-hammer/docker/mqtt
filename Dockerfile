# MQTT docker image.
#
# VERSION 0.0.2

FROM debian:latest

MAINTAINER Michael Hammer <https://github.com/michael-hammer>

ENV DEBIAN_FRONTEND noninteractive

# Install packages
RUN apt-get update && apt-get install -y mosquitto mosquitto-clients && apt-get clean

# Expose MQTT port
EXPOSE 1883
EXPOSE 8883

CMD ["/usr/sbin/mosquitto","-c","/etc/mosquitto/mosquitto.conf"]
